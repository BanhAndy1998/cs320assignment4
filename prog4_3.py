import prog4_1
import prog4_2
import sys

def main():
    print("Assignment #4-3, Andy Banh, Banhandyofficial1998@gmail.com")
    sma = prog4_2.StackMachine()
    userfile = open(sys.argv[1],'r')
    lines = userfile.read().splitlines() #Reads in a file with inputs seperated by spaces
    toParse = [] #Stores tokenized values and processed through StackMachine
    count = 0
    for i in lines:
        toParse.append(prog4_1.Tokenize(i))
    for i in toParse:
        prog4_1.Parse(i)
    
    while(sma.getCurrentLine() < len(toParse)):
        if(sma.getCurrentLine() < 0):
            print("Trying to execute invalid line:",sma.getCurrentLine())
            sys.exit(1)
        sma.Execute(toParse[sma.getCurrentLine()])
    
    print("Program terminated correctly")

main()
    