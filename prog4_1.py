import sys

tokenizeCheck = ["push","pop","add","sub","mul","div","mod","skip","save","get"]
parseCheck = ["push","save","get"]

def Tokenize(userIn): 
    tokenized = []
    for s in userIn.split():
        if(s.lstrip('-').isdigit()): #Removes the - sign in front of a integer for scanning
            tokenized.append(s)
            continue
        else:
            i = 0
            for t in tokenizeCheck:
                if(s == t):
                    i = 1
            if(i == 0):
                try:
                    raise ValueError
                except ValueError:
                    print("Unexpected token:",s)
                    sys.exit(1)
            tokenized.append(s)
    return tokenized

def Parse(tokens):
    x = 0
    while (x < len(tokens)-1):
        if(tokens[x].lstrip('-').isdigit()): 
            try:
                raise ValueError
            except ValueError:
                print("Parse error:",tokens[x])
                sys.exit(1)
        if (tokens[x] in parseCheck):
            if(x == len(tokens)-1):
                try:
                    raise ValueError
                except ValueError:
                    print("Parse error:",tokens[x])
                    sys.exit(1)
            if(tokens[x+1].lstrip('-').isdigit() == False): #If a digit does not follow, print error
                try:
                    raise ValueError
                except ValueError:
                    print("Parse error:",tokens[x],tokens[x+1])
                    sys.exit(1)
            x += 2
        else:                       
            if(x == len(tokens)-1):
                continue
            if(tokens[x+1].isdigit()):  #If a digit follows when it shouldn't, print error
                try:
                    raise ValueError
                except ValueError:
                    print("Parse error:",tokens[x],tokens[x+1])
                    sys.exit(1)
            x += 1
    return tokens
