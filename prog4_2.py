import sys

class StackMachine: 
    def __init__(self):
        self.stack = [] #Stores values which are modified by functions called through a user text file
        self.currentLine = 0 #Used to tell which line of input an error occured on and for completion of reading inputs
        self.storage = {} #Used for memorization of values for the duration of the program

    def getCurrentLine(self):
        return self.currentLine

    def Push(self,x): #Adds a value to the top of the stack
        self.stack.append(int(x))
        return None

    def Pop(self): #Retrieves top value off stack
        if(len(self.stack) == 0): #Error if no values on stack
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'pop'" + " caused Invalid Memory Access")
                        sys.exit(1)
        return self.stack.pop()
    
    def Add(self): #Adds the two top values of the stack and pushes it
        if(len(self.stack) < 2):
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'add'" + " caused Invalid Memory Access")
                        sys.exit(1)
        self.stack.append(self.stack.pop() + self.stack.pop())
        return None

    def Sub(self): #Subtracts the first popped value from the second and pushes it
        if(len(self.stack) < 2):
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'sub'" + " caused Invalid Memory Access")
                        sys.exit(1)
        self.stack.append(self.stack.pop() - self.stack.pop())
        return None

    def Mul(self): #Multiplies the two top values of the stack and pushes it
        if(len(self.stack) < 2):
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'mul'" + " caused Invalid Memory Access")
                        sys.exit(1)
        self.stack.append(self.stack.pop() * self.stack.pop())
        return None

    def Div(self): #Divides the second value on the stack from the first and pushes it
        if(len(self.stack) < 2):
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'div'" + " caused Invalid Memory Access")
                        sys.exit(1)    
        self.stack.append(self.stack.pop() / self.stack.pop())
        return None
    
    def Mod(self): #Gets the remainder of the Div(Self) operation and pushes it
        if(len(self.stack) < 2):
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'mod'" + " caused Invalid Memory Access")
                        sys.exit(1)
        self.stack.append(self.stack.pop() % self.stack.pop())
        return None

    def Skip(self): #Pops two values, if first is zero then add the second to currentLine, otherwise do nothing
        if(len(self.stack) < 2):
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'skip'" + " caused Invalid Memory Access")
                        sys.exit(1)
        if(self.stack.pop() == 0):
            self.currentLine += self.stack.pop()
        else:
            self.stack.pop()
        return None

    def Save(self,x): #Pops and stores the top value of the stack with x as a key
        if(len(self.stack) < 1):
                    try:
                        raise IndexError
                    except IndexError:
                        print("Line " + str(self.currentLine+1) + ": 'save'" + " caused Invalid Memory Access")
                        sys.exit(1)
        self.storage[x] = self.stack.pop()
        return None

    def Get(self,x): #Pushes a stored value at x, can push the same key multiple times
        if(self.storage[x] == None):
            try:
                raise IndexError
            except IndexError:
                print("Line " + str(self.currentLine+1) + ": 'get'" + " caused Invalid Memory Access")
                sys.exit(1)
        self.stack.append(self.storage[x])
        return None

    def Execute(self,tokens):
        x = 0
        while(x < len(tokens)):
            if(tokens[x] == "push"):
                x += 1
                self.Push(tokens[x])
            if(tokens[x] == "pop"):
                print(self.Pop())
            if(tokens[x] == "add"):
                self.Add()
            if(tokens[x] == "sub"):
                self.Sub()
            if(tokens[x] == "mul"):
                self.Mul()
            if(tokens[x] == "div"):
                self.Div()
            if(tokens[x] == "mod"):
                self.Mod()
            if(tokens[x] == "skip"):
                self.Skip()
            if(tokens[x] == "save"):
                x += 1
                self.Save(tokens[x])
            if(tokens[x] == "get"):
                x += 1
                self.Get(tokens[x])
            x += 1
        self.currentLine += 1