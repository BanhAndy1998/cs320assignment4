Name : Andy Banh
Email : Banhandyofficial1998@gmail.com
REDID : 820256990

Overview - Program 4 is programmed using Python 3. This program is designed to Tokenize and Parse a user inputted text file and process it onto a stack machine.

prog4_1.py - Prog4_1 contains the definitions for the tokenize and parse functions. The tokenize function checks that a space delimited string from the input is either an positive or negative integer or any of the following strings: push, pop, add, sub, mul, div, sub, mod, skip, save, or get. The parse function takes a list in and checks for input correctness. The push, save, and get should have integers following them while any other arguments should not. If an error is met, the program will stop and print a message with the offending argument.

prog4_2.py - Prog4_2 contains a Stack Machine class that is designed to process the list of arguments the user has inputted after it has been tokenized and parsed. The operations that the stack machine can do are as follows:

Push - Push adds a value to the stack.  
Pop - Removes and prints the topmost value of the stack.  
Add - Pops and adds the two topmost values of the stack and pushes it back on.  
Sub - Pops two values and subtracts the second from the first and pushes it back on.  
Mul - Pops and multiplies the two topmost values of the stack and pushes it back on.  
Div - Pops two values and divides the second from the first and pushes it back on.  
Mod - Does the same as the above function except pushes the remaind instead.  
Skip - Pops two values. If the first value is zero, Move forward or backwards as many lines as the second value popped in the text file . Otherwise do nothing.  
Save - Pops a value off the stack and stores it using the next token as a key until the program has finished executing.  
Get - Pushes a stored value using the next token as a key. The stored value can be pushed multiple times until the program finishes executing.  

Should any of the above functions be called when there are not enough values on the stack or a non-indexed value is accessed then the program will stop and a error message will be printed

prog4_3.py - Prog4_3 is a driver program for Prog4_2 and Prog4_1. The program takes in a string argument through the command line which should point to a text file. It will then tokenize, parse, and perform all functions on the text file until an error occurs or the program successfully finishes executing.